import struct
import asyncio
from bleak import BleakClient, BleakScanner, BleakError
# effectuer d'abord un scan pour déterminer votre adress MAC
"""
async def scan():
    scanner = BleakScanner()
    
    await scanner.start()
    print("Scanning for BLE devices...")

    await asyncio.sleep(10)

    await scanner.stop()
    print("Scan stopped.")

    devices = await scanner.get_discovered_devices()
    
    for device in devices:
        print(f"Device found: {device}")

asyncio.run(scan())

"""
#mettre votre adresse MAC ici
mac_address = "EE:76:D4:91:57:1A"

async def get_device(mac_addresse):
    device = await BleakScanner.discover()
    for d in device :
        if d.address == mac_addresse : 
            return d
            
def decode_string(data):
    format_string = '8s I I Q'
    decoded_data = struct.unpack(format_string, data)
    name = decoded_data[0].decode().rstrip('\x00')
    size = decoded_data[1]
    hash_value = decoded_data[2]
    creation_date = decoded_data[3]
    return {'name': name, 'size': size, 'hash': hash_value, 'creation_date': creation_date}

class DataStructure:
    def __init__(self):
        self.name = ''
        self.chunk_id = 0
        self.fin_chunk_id = 0

    def set_from_byte_array(self, byte_array):
        if len(byte_array) >= 16:  
            self.name, self.chunk_id, self.fin_chunk_id = struct.unpack('8s I I', byte_array[:16])
        else:
            raise ValueError("Byte array is too short to parse")

    def to_byte_array(self):
        return struct.pack('8s I I', self.name, self.chunk_id, self.fin_chunk_id)

structure = DataStructure()
structure.name = b'T6700'
structure.chunk_id = 0
structure.fin_chunk_id = 9999

byte_array = structure.to_byte_array()
async def run():
    device = await get_device(mac_address)
    try:
        async with BleakClient(mac_address) as client:
            if client.is_connected:
                print("Connected")
                services = await client.get_services()
                for service in services:
                    print(service)                
                    for charac in service.characteristics:
                        if (charac.uuid in ["1b0d1303-a720-f7e9-46b6-31b601c4fca1","1b0d1407-a720-f7e9-46b6-31b601c4fca1","1b0d1302-a720-f7e9-46b6-31b601c4fca1", "1b0d140a-a720-f7e9-46b6-31b601c4fca1","1b0d1304-a720-f7e9-46b6-31b601c4fca1"]):
                                print(charac)
                                await client.start_notify(charac.uuid, notification_callback)
                                try:
                                    data = await client.read_gatt_char(charac)
                                    print("Read data:", data)
                                except BleakError as e:
                                    print(f"Error reading characteristic {charac.uuid}: {e}")
                                    num = await client.read_gatt_char("1b0d1303-a720-f7e9-46b6-31b601c4fca1")                                   
                #ecrire dans LIST le NUM recupere                    
                await client.write_gatt_char("1b0d1302-a720-f7e9-46b6-31b601c4fca1", num)
                #ecrire le fichier dans read 
                await client.write_gatt_char("1b0d1304-a720-f7e9-46b6-31b601c4fca1",byte_array)
                        
                await asyncio.sleep(10)
                await client.stop_notify(charac.uuid)
    except Exception as e:
        print(e)

def notification_callback(sender: int, data: bytearray):
    if (sender == 111):
        print(f"Notification received from  LOG {sender}: {data}")
    if (sender == 58):
        print(f"Notification received from  LIST {sender}: {decode_string(data)}")
    if (sender == 66):
        print(f"Notification received from  READ {sender}: {data}")
        with open('data.hex','wb') as f:
            f.write(data)        
    if (sender == 100):
        print(f"Notification received from  ACK {sender}: {data}")

asyncio.run(run())